export const API_RESOURCE_CREATE = 'https://6ytulhl121.execute-api.us-east-1.amazonaws.com/create'      //post
export const API_RESOURCE_DESTROY = 'https://6ytulhl121.execute-api.us-east-1.amazonaws.com/destroy'    //post
export const API_STATUS_CHECK = 'https://6ytulhl121.execute-api.us-east-1.amazonaws.com/status'         //get
export const API_TERRA_SCRIPT = 'https://6ytulhl121.execute-api.us-east-1.amazonaws.com/config' //get