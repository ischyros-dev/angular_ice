export enum routes {
    LOGIN = '/login',
    RESOURCE_PAGE = '/resource-page',
    ERROR_404_PAGE = '/404',
    ERROR_500_PAGE = '/500'
}
