import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { NotFoundComponent, InternalServerErrorComponent } from './pages/errors/index';

const routes: Routes = [
  {
    path: '', loadChildren: () => import('./pages/auth/auth.module')
    .then(m => m.AuthModule)
  },
  {
    path: 'resource-page', loadChildren: () => import('./pages/resource/resource.module')
    .then(m => m.ResourceModule)
  },
  { path: '404', component: NotFoundComponent },
  { path: '500', component: InternalServerErrorComponent },
  { path: '**', redirectTo: '/404', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
    relativeLinkResolution: 'legacy'
  })
],
  exports: [RouterModule]
})
export class AppRoutingModule { }
