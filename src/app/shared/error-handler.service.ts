import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { routes } from '../consts/routes'

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor(
    private router: Router
  ) { }

  errorMessage: string =''
  routers: typeof routes = routes

  handleError = (error: HttpErrorResponse) => {
    if(error.status === 500) {
      this.handle500Error(error)
    }
    else if(error.status === 404) {
      this.handle404Error(error)
    }
    else {
      this.handleError(error)
    }
  }

  private handle500Error = (error: HttpErrorResponse) => {
    this.createErrorMessage(error);
    this.router.navigate([this.routers.ERROR_500_PAGE]).then();
  }

  private handle404Error = (error: HttpErrorResponse) => {
    this.createErrorMessage(error);
    this.router.navigate([this.routers.ERROR_404_PAGE]).then();
  }

  private createErrorMessage = (error: HttpErrorResponse) => {
    this.errorMessage = error.error ? error.error : error.statusText;
  }
}
