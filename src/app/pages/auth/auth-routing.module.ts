import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AuthPageComponent } from './containers/auth-page/auth-page.component';

const routes: Routes = [
  { path: 'user-auth', component: AuthPageComponent },
  { path: '', redirectTo: 'user-auth', pathMatch: 'full' }
]


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class AuthRoutingModule { }
