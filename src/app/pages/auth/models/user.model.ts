export interface User {
    emailAddress: string
    password: string
    firstName: string
    lastName: string
}