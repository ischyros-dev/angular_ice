import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  signup() {
    localStorage.setItem('token', 'token')
  }

  login() {
    localStorage.setItem('token', 'token')
  }

  logout() {
    localStorage.removeItem('token')
  }

  getCurrentUser(): Observable<User> {
    return of({
      emailAddress: 'jim.lorica@fisglobal.com',
      password: 'admin',
      firstName: 'Jim',
      lastName: 'Lorica'
    })

  }
}
