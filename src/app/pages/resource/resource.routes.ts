import { Routes } from "@angular/router";
import { ResourcePageComponent } from "./containers/resource-page/resource-page.component";

export const resourceRoutes: Routes = [
    { path: '', component: ResourcePageComponent }
]
