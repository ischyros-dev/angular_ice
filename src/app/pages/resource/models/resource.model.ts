export interface Resource {
  cloudPlatform: string,
  resourceGroupName: string,
  location: string,
  virtualNetworkAddressSpace: string,
  subnetAddressPrefix: string,
  ipAddressAllocation: string,
  imageId: string,
  instanceType: string,
  tableName: string,
  readCapacity: number,
  writeCapacity: number,
  partitionKey: any
}
