import { Component, OnInit } from '@angular/core';
import { ResourcePageService } from '../../services/resource-page.service';

@Component({
  selector: 'app-resource-page',
  templateUrl: './resource-page.component.html',
  styleUrls: ['./resource-page.component.scss']
})
export class ResourcePageComponent implements OnInit {

  constructor(public service: ResourcePageService) { }

  ngOnInit(): void {
  }

}
