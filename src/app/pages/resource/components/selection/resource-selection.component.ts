import { Component, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {CdkDragDrop, CdkDragExit, CdkDragEnter, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { MatDialog } from '@angular/material/dialog';
import { ResourcePageService } from '../../services/resource-page.service';
import { ResourceConfirmationComponent } from '../confirmation/resource-confirmation.component';
import { Resource } from '../../models/resource.model';
import { HttpClient } from '@angular/common/http';
import { API_TERRA_SCRIPT } from 'src/app/consts/env';


@Component({
  selector: 'app-resource-selection',
  templateUrl: './resource-selection.component.html',
  styleUrls: ['./resource-selection.component.scss']
})
export class ResourceSelectionComponent {
  resourceSelection: Resource

  @Input() productCost: number
  totalProductCost: number = 0

  constructor(
    public dialog: MatDialog,
    private http: HttpClient,
    private resourcePageService: ResourcePageService
    ) { }

  form: FormGroup = new FormGroup({
    cloudPlatform: new FormControl('', [Validators.required]),
    resourceGroupName: new FormControl('', [Validators.required]),
    location: new FormControl('', [Validators.required]),
    virtualNetworkAddressSpace: new FormControl({ value: '10.0.0.0/16', disabled: true }),
    subnetAddressPrefix: new FormControl({ value: '10.0.2.0/24', disabled: true }),
    ipAddressAllocation: new FormControl({ value: 'Dynamic', disabled: true }),
  });


  amazonForm: FormGroup = new FormGroup({
    imageId: new FormControl('',[Validators.required]),
    instanceType: new FormControl('',[Validators.required]),
    subNet: new FormControl({ value: '10.0.2.0/24', disabled: true }),
    memory: new FormControl('',[Validators.required]),
    cpu: new FormControl('',[Validators.required]),
  });

  dynamoDbForm: FormGroup = new FormGroup({
    tableName: new FormControl('',[Validators.required]),
    readCapacity: new FormControl('',[Validators.required]),
    writeCapacity: new FormControl('',[Validators.required]),
    partitionKey: new FormControl('',[Validators.required]),
  });


  available = [
    {
      "name":"Amazon EC2",
      "image":"../../../../../assets/images/ec2_thumbnail.jpg",
    },
    {
      "name":"Dynamo DB",
      "image":"../../../../../assets/images/dynamodb_icon.png"
    },
    {
      "name":"Amazon S3",
      "image":"../../../../../assets/images/amazons3_icon.png"
    },
    {
      "name":"Elastic Cache",
      "image":"../../../../../assets/images/awsec_icon.png"
    },
    {
      "name":"Load Balancer",
      "image":"../../../../../assets/images/awslb_icon.png"
    },
  ];

  assigned = [];

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }

  public resourceGroup:any;
  valuechange(lbl, newValue) {
    this.resourceGroup = newValue;
    document.getElementById(lbl).innerHTML = "Note: Please drag and drop resources you wish to create in "
    +this.resourceGroup+" resource group.";
  }


  //drag and drop events
  formShow: boolean = false ; // hidden by default
  formShow2: boolean = false ; // hidden by default

  entered(event: CdkDragEnter<string[]>) {
    if(event.item.data.toString() == "Amazon EC2"){
      this.formShow= ! true;
      this.amazonForm.reset();
    }
    if(event.item.data.toString() == "Dynamo DB"){
      this.formShow2 = ! true;
      this.dynamoDbForm.reset();
    }
  }

  exited(event: CdkDragExit<string[]>) {
    if(event.item.data.toString() == "Amazon EC2"){
      this.formShow = ! false;
      this.formShow2 = ! true;
    }
    if(event.item.data.toString() == "Dynamo DB"){
      this.formShow2 = ! false;
      this.formShow = ! true;
    }
  }

  calculateCost(){
    let ec2ImageId = this.amazonForm.get('imageId').value;
    let ec2InstanceType = this.amazonForm.get('instanceType').value;
    let dynamoDbReadCapacity = this.dynamoDbForm.get('readCapacity').value;
    let dynamoDbWriteCapacity = this.dynamoDbForm.get('writeCapacity').value;

    this.getProductCost(ec2ImageId, ec2InstanceType, dynamoDbReadCapacity, dynamoDbWriteCapacity);
  }


  setMonthlyCost(cost){
    console.log(cost);
    document.getElementById("monthlyCost").innerHTML = "Total Monthly Cost: $"+cost;
  }

  getProductCost(ec2ImageId, ec2InstanceType, dynamoDbReadCapacity, dynamoDbWriteCapacity){
    if(ec2ImageId === 'Amazon Linux 2' && ec2InstanceType === 't2.micro') {
      this.setMonthlyCost("8.26")
    }
    if(ec2ImageId === 'Amazon Linux 2' && ec2InstanceType === 't2.micro' && dynamoDbReadCapacity === "500" && dynamoDbWriteCapacity === "500") {
      this.setMonthlyCost("8.65")
    }
    if(ec2ImageId === 'Amazon Linux 2' && ec2InstanceType === 't2.micro' && dynamoDbReadCapacity === "1000" && dynamoDbWriteCapacity === "500") {
      this.setMonthlyCost("8.715");
    }
    if(ec2ImageId === 'Amazon Linux 2' && ec2InstanceType === 't2.micro' && dynamoDbReadCapacity === "1000" && dynamoDbWriteCapacity === "1000") {
      this.setMonthlyCost("9.04");
    }
    if(ec2ImageId === 'Amazon Linux 2' && ec2InstanceType === 't2.micro' && dynamoDbReadCapacity === "500" && dynamoDbWriteCapacity === "1000") {
      this.setMonthlyCost("8.975");
    }
    if(ec2ImageId === 'MS Windows 2022 Server' && ec2InstanceType === 't2.micro' && dynamoDbReadCapacity === "500" && dynamoDbWriteCapacity === "500") {
      this.setMonthlyCost("12");
    }
    if(ec2ImageId === 'MS Windows 2022 Server' && ec2InstanceType === 't2.micro' && dynamoDbReadCapacity === "1000" && dynamoDbWriteCapacity === "500") {
      this.setMonthlyCost("12.065");
    }
    if(ec2ImageId === 'MS Windows 2022 Server' && ec2InstanceType === 't2.micro' && dynamoDbReadCapacity === "1000" && dynamoDbWriteCapacity === "1000") {
      this.setMonthlyCost("12.39")
    }
    if(ec2ImageId === 'MS Windows 2022 Server' && ec2InstanceType === 't2.micro' && dynamoDbReadCapacity === "500" && dynamoDbWriteCapacity === "1000") {
      this.setMonthlyCost("12.325");
    }

  }

  executeInfra(formValues) {
    this.resourcePageService.createResource(formValues).subscribe()
  }

  openDialog() {
    this.dialog.open(ResourceConfirmationComponent);
    // this.executeInfra(formValues)
  }

  downloadTerra(){
    let data = '';
    fetch('https://6ytulhl121.execute-api.us-east-1.amazonaws.com/config').then(function(response) {
        return response.json();
      }).then(function(myJson) {

    var fileContents = JSON.stringify(myJson, null, 2);
    var a = document.createElement("a");
    var dataURI = "data:" + "text/plain" +";base64," + btoa(fileContents);
    a.href = dataURI;
    a['download'] = "Terraform Script";
    var e = document.createEvent("MouseEvents");
    //deprecated function but needed to satisfy TypeScript.
    e.initMouseEvent("click", true, false,
        document.defaultView, 0, 0, 0, 0, 0,
        false, false, false, false, 0, null);
    a.dispatchEvent(e);
   });
  }

}
