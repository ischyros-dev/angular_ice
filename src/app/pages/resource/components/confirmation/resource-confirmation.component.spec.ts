import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceConfirmationComponent } from './resource-confirmation.component';

describe('ResourceConfirmationComponent', () => {
  let component: ResourceConfirmationComponent;
  let fixture: ComponentFixture<ResourceConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResourceConfirmationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
