import { Component, OnInit } from '@angular/core';
import { ResourcePageService } from 'src/app/pages/resource/services/resource-page.service';
import { Resource } from '../../models/resource.model'

@Component({
  selector: 'app-resource-confirmation',
  templateUrl: './resource-confirmation.component.html',
  styleUrls: ['./resource-confirmation.component.scss']
})
export class ResourceConfirmationComponent implements OnInit {

  constructor(
    private resourcePageService: ResourcePageService
  ) { }

  resourceGroupName = 'InnovateON'
  imageId = 'Amazon Linux 2'
  virtualNetworkAddressSpace = '10.0.0.0/16'
  subnetAddressPrefix = '10.0.2.0/24'
  instanceType = 't2.micro'
  location = 'us-east-1'
  tableName = 'InnovateONTableSample'
  readCapacity = 500
  writeCapacity = 500
  partitionKey = 'id'

  statusResponse: string = ''
  statusShow: boolean = false ;

  toggleShow() {
    this.statusShow = ! this.statusShow
    this.resourcePageService.getStatus()
    .subscribe((res => {
      this.statusResponse = JSON.stringify(res.json())
    }))
  }

  executeDummyInfra() {
    this.resourcePageService.dummyCreate().subscribe()
  }

  executeInfra() {

  }

  ngOnInit(): void {
  }

}
