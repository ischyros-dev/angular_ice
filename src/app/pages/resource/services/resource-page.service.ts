import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ErrorHandlerService } from '../../../shared/error-handler.service';
import {
  API_RESOURCE_CREATE,
  API_RESOURCE_DESTROY,
  API_STATUS_CHECK
} from 'src/app/consts/env';
import { Resource } from '../models/resource.model';

@Injectable({
    providedIn: 'root'
})

export class ResourcePageService {

  constructor(
    private router: Router,
    private http: HttpClient,
    private errorHandler: ErrorHandlerService
    ) { }

  errorMessage: string = ''

  getStatus(): Observable<any> {
    return this.http.get<any>(`${API_STATUS_CHECK}`)
  }

  resourceCreation: Resource
  createResource(resourceCreation) {
    let resourceProperty = {
      "infra_provider": {
        "provider": "aws",
        "region": "us-east-1"
      },
      "infra_vpc": {
        "name": "ioclick_vpc",
        "cidr_block": "10.0.0.0/16"
      },
      "infra_subnet": {
        "name": "ioclick_subnet",
        "cidr_block": "10.0.2.0/24"
      },
      "infra_server": {
        "name": "resourceCreation.resourceGroupName",
        "image_id": "resourceCreation.imageId",
        "instance_type": "resourceCreation.instanceType"
      },
      "infra_database": {
        "name": "Project ICE Test DB",
        "table_name": "resourceCreation.tableName",
        "read_capacity": resourceCreation.readCapacity,
        "write_capacity": resourceCreation.writeCapacity,
        "partition_key": {
            "name": "resourceCreation.partitionKey",
            "type": "N"
        }
      }
    }
    let options = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    }
    return this.http.post<Resource>(`${API_RESOURCE_CREATE}`, resourceProperty, options)
    .pipe(map(data => {
      this.resourceCreation = <Resource>data['create']
    },
    (error) => {
      this.errorHandler.handleError(error)
      this.errorMessage = this.errorHandler.errorMessage
    }))
  }

  resourceDestroy: Resource
  destroyResource(resourceDestroy) {
    return 'TODO'
  }

  dummyCreate() {
    let dummyProperty = {
      "infra_provider": {
          "provider": "aws",
          "region": "us-east-1"
      },
      "infra_vpc": {
          "name": "ioclick_vpc",
          "cidr_block": "10.0.0.0/16"
      },
      "infra_subnet": {
          "name": "ioclick_subnet",
          "cidr_block": "10.0.2.0/24"
      },
      "infra_server": {
          "name": "InnovateON",
          "image_id": "Amazon Linux 2",
          "instance_type": "t2.micro"
      },
      "infra_database": {
          "name": "InnovateONTestDB",
          "table_name": "InnovateONTableSample",
          "read_capacity": 500,
          "write_capacity": 500,
          "partition_key": {
              "name": "id",
              "type": "N"
          }
      }
  }
    let options = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) }

    return this.http.post(`${API_RESOURCE_CREATE}`, dummyProperty, options)
    .pipe(map(data => {
      console.log(data)
    }))
  }



}
