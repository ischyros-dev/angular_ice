import { Component, OnInit } from '@angular/core';
import { routes } from '../../consts/routes';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  routes: typeof routes = routes

}
