import { Component, OnInit } from '@angular/core';
import { routes } from '../../consts/routes';

@Component({
  selector: 'app-internal-server-error',
  templateUrl: './internal-server-error.component.html',
  styleUrls: ['./internal-server-error.component.scss']
})
export class InternalServerErrorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  routes: typeof routes = routes

  errorMessage: string = 'Internal Server Error. Please contact administrator!'

}
