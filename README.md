## **Solution Name**: **InfraOnClick by Project I.C.E.**
### Solution Description
> **InfraOnClick** is a powerful combination of Low-code/No-code and Infrastructure-As-Code (IAC) concepts designed for users to create Infrastructure on Cloud Platforms easily on single click by using a drag and drop feature. It integrates an added feature to download terraform script of the desired infrastructure.

### Target Audience
>- Non-technical users (Business Analysts, Office Administrators, small-business owners and others who are not software developers to build and test applications)
>- Technical Users (Developers, DevOps)
>- Basically anyone from an HR manager to a professional developer can use low-code to develop solutions. Their intuitive nature and scalability make it possible for anyone within a company to create tools to automate their work.

### Solution Design
We are providing following modules as a part of the project **InfraOnClick**.
>- **Login**
>- **Resource Selection Screen**: On this screen user can provide their inputs by drag and drop feature from left hand side to right hand side. We are also displaying the properties of the selected inputs resources like EC2, DynamoDB, S3 etc. on right hand side section. Some of the resource properties are kept as default while others take user input. ***Project I.C.E. introduced cost estimation in this section***
>- **Resource Confirmation Screen**: On this screen user can see and confirm the selected resource properties along with infrastructure relationship diagram and proceed to create required infrastructure. User can see the success message on the  screen and can navigate to destination url to see the infrastructure.

### Technologies and Architecture used
>- Serverless Architecture
    -AWS Lambda (Python Boto3)
    -S3
    -API Gateway
>- AWS EC2 - (Free trial subscription)
>- RUF v5.2 (Responsive UI Framework) - (FIS Internal Framework)
>- Angular - (OpenSource)
>- Terraform v1.09 - (OpenSource)
>- Serverless Framework 2.61.0 - (OpenSource)
>- Bitbucket Pipelines CI/CD - (OpenSource)

### Solution Features
>- Serverless Architecture with CI/CD.
>- Monthly Cost Estimate for Resources.
>- Drag and Drop Resources.
>- Automate Infrastructure creation on single click of button.

### Why is it cool?
>- It is a Low-Code/No-Code based Infrastructure as Code Tool.
>- Single Click Infrastructure creation.
>- Minimum technical knowledge required.
>- Introduced cost estimation to help users manage and decide which resources to choose.
>- Cheaper overhead due to serverless architecture.

### Limitations 
>- Syncing Cloud Platform latest features with the tool.
>- Only running on AWS right for now.
>- Resources in scope are only EC2 and DynamoDB.

### Future Scope
>- Support can be extended for all Cloud Platforms.
>- Additional integration of all major Cloud Resources, Services and Platforms.
>- Multiple instances creation of a selected resource.



### Build Instructions
FRONTEND
>- Install latest version of 'nodejs' and 'aws cli'.
>- Clone the repository.
>- ```npm install -g @angular/cli```
>- ```npm install``` Note: Execute this command while connected to FIS network, since RUF will be installed together with other dependency modules.
>- ```npm start```

BACKEND
>- terraform cli
>- aws cli
>- nodejs
>- serverless cli
>- python
>- Clone the repository.

### Primary Developers and Responsibilities
>- **Edward Henry Recio** (Captain): Terraform integration 
>- **Arthur Olano**: Application Design & Setup (AWS)
>- **Jemar Lorica**: UI Design & Coding - RUF and Angular
>- **Jeff Ynota**: UI Design & Coding - RUF and Angular
>- **Dexter Paje**: Python Development (AWS Lambda)

### References 
>- [Terraform Docs](https://www.terraform.io/docs/index.html)
>- [RUF - FIS Responsive UI Framework](https://wiki.fnis.com/x/Ih3FAg)
